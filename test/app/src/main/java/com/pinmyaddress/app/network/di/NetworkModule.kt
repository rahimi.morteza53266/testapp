package com.pinmyaddress.app.network.di


import com.google.gson.GsonBuilder
import kotlinx.coroutines.runBlocking
import okhttp3.Dispatcher
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import java.io.IOException


val networkModule = module {

    factory { provideGsonConverterFactory() }
    factory { provideConverterFactory() }
    factory {
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }

    factory { provideOkHttpClient(get()) }
    factory(named("withWrapper")) { (url: String) ->
        provideRetrofit(
            get(),
            get(),
            url
        )
    }

}

fun provideGsonConverterFactory(): GsonConverterFactory {
    val gson = GsonBuilder()
        .setLenient()
        .create()
    return GsonConverterFactory.create(gson)
}

fun provideConverterFactory(): Converter.Factory =
    GsonConverterFactory.create(GsonBuilder().setLenient().create())


fun provideOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
    val dispatcher = Dispatcher()
    dispatcher.maxRequests = 1
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
    return OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor)
        .dispatcher(dispatcher = dispatcher).build()
}

fun provideRetrofit(
    okHttpClient: OkHttpClient,
    converterFactory: Converter.Factory,
    url: String
): Retrofit =
    Retrofit.Builder()
        .client(okHttpClient)
        .baseUrl(url)
        .addConverterFactory(converterFactory)
        .build()

