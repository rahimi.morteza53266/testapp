package com.pinmyaddress.app.data.model

data class Northeast(
    val lat: Double,
    val lng: Double
)