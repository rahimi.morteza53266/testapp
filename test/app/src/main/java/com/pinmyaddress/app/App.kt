package com.pinmyaddress.app

import android.app.Application
import android.content.Context
import com.huawei.agconnect.config.AGConnectServicesConfig
import com.huawei.agconnect.config.LazyInputStream
import com.huawei.hms.maps.MapsInitializer
import com.pinmyaddress.app.di.geoCodeModule
import com.pinmyaddress.app.network.di.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.Koin
import org.koin.core.context.startKoin
import java.io.IOException
import java.io.InputStream



class App: Application() {

    override fun onCreate() {
        super.onCreate()
        MapsInitializer.initialize(this)

        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(
                listOf(
                    networkModule,
                    geoCodeModule
                )
            )
        }
    }

    override fun attachBaseContext(context: Context?) {
        super.attachBaseContext(context)
        val config = AGConnectServicesConfig.fromContext(context)
        config.overlayWith(object : LazyInputStream(context) {
            override fun get(context: Context): InputStream? {
                return try {
                    context.assets.open("agconnect-services.json")
                } catch (e: IOException) {
                    null
                }
            }
        })
    }
}