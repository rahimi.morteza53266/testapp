package com.pinmyaddress.app.network

import android.util.Log
import retrofit2.Response

object Api {


    inline fun <reified T : Any> result(service: () -> Response<T>): Result<T> {
        return try {

            val result = service()
            if (result.isSuccessful) {
                Result.Success(result.body()!!)
            } else {
                Result.GenericError(result.code(), result.errorBody()?.string())
            }
        } catch (e: Exception) {
            Log.e("Network Errors", e.message, e)
            Result.NetworkError
        }
    }


}