package com.pinmyaddress.app.data.model

data class Location(
    val lat: Double,
    val lng: Double
)