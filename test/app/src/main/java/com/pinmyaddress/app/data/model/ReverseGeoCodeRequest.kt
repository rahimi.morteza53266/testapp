package com.pinmyaddress.app.data.model

data class ReverseGeoCodeRequest(
    val location: LocationX
)