package com.pinmyaddress.app.network

data class CallBackHandler(
    var showLoading: Boolean,
    var isSuccess: Boolean,
    var message: String,
    var lat: Double,
    var lng: Double
)