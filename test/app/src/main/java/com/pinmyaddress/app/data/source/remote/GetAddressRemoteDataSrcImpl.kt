package com.pinmyaddress.app.data.source.remote

import com.huawei.hms.support.api.client.ApiClient
import com.pinmyaddress.app.data.model.ReverseGeoCodeRequest
import com.pinmyaddress.app.data.model.ReverseGeoCodeResponse
import com.pinmyaddress.app.network.Api
import com.pinmyaddress.app.network.BaseResponse
import com.pinmyaddress.app.network.Result
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetAddressRemoteDataSrcImpl(
    private val api: GeoApi,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
): GetAddressRemoteDataSrc {
    override suspend fun getAddress(
        requestGeoCodeRequest: ReverseGeoCodeRequest,
        key: String
    ): Result<BaseResponse<ReverseGeoCodeResponse>> {
        return withContext(dispatcher) {
            return@withContext Api.result {
                api.getAddress(requestGeoCodeRequest,key)
            }
        }
    }


}