package com.pinmyaddress.app.di

import com.pinmyaddress.app.data.source.GetAddressRepository
import com.pinmyaddress.app.data.source.GetAddressRepositoryImpl
import com.pinmyaddress.app.data.source.remote.GeoApi
import com.pinmyaddress.app.data.source.remote.GetAddressRemoteDataSrc
import com.pinmyaddress.app.data.source.remote.GetAddressRemoteDataSrcImpl
import com.pinmyaddress.app.network.ApiConstants
import com.pinmyaddress.app.ui.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

val geoCodeModule = module {

    viewModel { MainViewModel(get()) }

    factory {
        provideSplashApi(get(
            named("withWrapper"),
            parameters = { parametersOf("https://siteapi.cloud.huawei.com") }
        ))
    }

    single<GetAddressRemoteDataSrc> { GetAddressRemoteDataSrcImpl(get()) }

    factory<GetAddressRepository> {
        GetAddressRepositoryImpl(
            get()
        )
    }
}


fun provideSplashApi(retrofit: Retrofit): GeoApi = retrofit.create(
    GeoApi::class.java
)
