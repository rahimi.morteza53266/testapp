package com.pinmyaddress.app.data.model

data class Address(
    val adminArea: String,
    val country: String,
    val countryCode: String,
    val locality: String,
    val subAdminArea: String
)