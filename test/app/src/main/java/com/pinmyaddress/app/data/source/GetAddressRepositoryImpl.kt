package com.pinmyaddress.app.data.source

import com.pinmyaddress.app.data.model.ReverseGeoCodeRequest
import com.pinmyaddress.app.data.model.ReverseGeoCodeResponse
import com.pinmyaddress.app.data.source.remote.GetAddressRemoteDataSrc
import com.pinmyaddress.app.network.BaseResponse
import com.pinmyaddress.app.network.Result

class GetAddressRepositoryImpl(
    private val getAddressRemoteDataSrc: GetAddressRemoteDataSrc
) : GetAddressRepository {
    override suspend fun getAddress(
        requestGeoCodeRequest: ReverseGeoCodeRequest,
        key: String
    ): Result<BaseResponse<ReverseGeoCodeResponse>> {
        return getAddressRemoteDataSrc.getAddress(requestGeoCodeRequest, key)
    }
}