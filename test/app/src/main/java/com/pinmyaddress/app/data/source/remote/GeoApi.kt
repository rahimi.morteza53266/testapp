package com.pinmyaddress.app.data.source.remote

import com.pinmyaddress.app.network.ApiConstants
import com.pinmyaddress.app.data.model.ReverseGeoCodeRequest
import com.pinmyaddress.app.data.model.ReverseGeoCodeResponse
import com.pinmyaddress.app.network.BaseResponse
import retrofit2.Response
import retrofit2.http.*

interface GeoApi {

    @Headers("Content-Type: application/json")
    @POST(ApiConstants.REVERSE_GEO_CODING)
    suspend fun getAddress(
        @Body reverseGeoCodeRequest: ReverseGeoCodeRequest,
        @Query("key") key: String
    ): Response<BaseResponse<ReverseGeoCodeResponse>>
}