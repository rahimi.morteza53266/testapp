package com.pinmyaddress.app.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pinmyaddress.app.data.model.LocationX
import com.pinmyaddress.app.data.model.ReverseGeoCodeRequest
import com.pinmyaddress.app.data.model.Site
import com.pinmyaddress.app.data.source.GetAddressRepository
import com.pinmyaddress.app.data.source.remote.GetAddressRemoteDataSrc
import com.pinmyaddress.app.network.CallBackHandler
import com.pinmyaddress.app.network.Result
import kotlinx.coroutines.launch

class MainViewModel(private val getAddressRepository: GetAddressRepository) : ViewModel() {

    private val KEY: String =
        "CV8HOkhPbRnJdtx7M41XbAC1UqO9figszZH41kquJjCW44rc0Bm1XyzfXkl3+1dqEYUapB5qWgYEAmss18NNOKvxYmG7"
    private val _observeLoading = MutableLiveData<CallBackHandler>()
    val observeLoading: LiveData<CallBackHandler> = _observeLoading

    private val _sites = MutableLiveData<List<Site>>()
    val sites: LiveData<List<Site>> = _sites

    fun getAddress(lat: Double, lng: Double) {

        val reverseGeoCodeRequest = ReverseGeoCodeRequest(LocationX(lat, lng))
        viewModelScope.launch {

            when (val res = getAddressRepository.getAddress(reverseGeoCodeRequest, KEY)) {
                is Result.Success -> {

                    _sites.value = res.data.sites?.sites
                    _observeLoading.postValue(
                        CallBackHandler(
                            showLoading = false,
                            isSuccess = true,
                            message = "Api worked.",
                            lat = lat,
                            lng = lng
                        )
                    )
                }

                is Result.GenericError -> {
                    _observeLoading.postValue(
                        CallBackHandler(
                            showLoading = false,
                            isSuccess = false,
                            message = res.message.toString(),
                            lat = lat,
                            lng = lng
                        )
                    )
                }

                is Result.NetworkError -> {
                    _observeLoading.postValue(
                        CallBackHandler(
                            showLoading = false,
                            isSuccess = false,
                            message = "Checking your network...",
                            lat = lat,
                            lng = lng
                        )
                    )
                }
            }

        }
    }
}