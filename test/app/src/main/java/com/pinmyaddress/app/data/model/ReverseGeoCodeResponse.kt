package com.pinmyaddress.app.data.model

data class ReverseGeoCodeResponse(
    val returnCode: String,
    val returnDesc: String,
    val sites: List<Site>
)