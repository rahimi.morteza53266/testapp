package com.pinmyaddress.app.data.model

data class LocationX(
    val lat: Double,
    val lng: Double
)