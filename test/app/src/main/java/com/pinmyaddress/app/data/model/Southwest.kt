package com.pinmyaddress.app.data.model

data class Southwest(
    val lat: Double,
    val lng: Double
)