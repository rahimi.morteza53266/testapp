package com.pinmyaddress.app.network

import retrofit2.Call
import retrofit2.http.*

object ApiConstants {
     const val REVERSE_GEO_CODING = "/mapApi/v1/siteService/reverseGeocode"

}