package com.pinmyaddress.app.data.source

import com.pinmyaddress.app.data.model.ReverseGeoCodeRequest
import com.pinmyaddress.app.data.model.ReverseGeoCodeResponse
import com.pinmyaddress.app.network.BaseResponse
import com.pinmyaddress.app.network.Result

interface GetAddressRepository {
    suspend fun getAddress(requestGeoCodeRequest: ReverseGeoCodeRequest, key:String): Result<BaseResponse<ReverseGeoCodeResponse>>
}