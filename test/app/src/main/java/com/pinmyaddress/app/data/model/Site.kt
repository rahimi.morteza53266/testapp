package com.pinmyaddress.app.data.model

data class Site(
    val address: Address,
    val formatAddress: String,
    val location: Location,
    val name: String,
    val siteId: String,
    val viewport: Viewport
)