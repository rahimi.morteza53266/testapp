package com.pinmyaddress.app.network


data class BaseResponse<TResponse>(var sites: TResponse?) {
    var returnCode: String = ""
    var returnDesc: String = ""
}

