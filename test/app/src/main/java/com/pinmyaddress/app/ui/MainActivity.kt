package com.pinmyaddress.app.ui

import android.Manifest
import android.content.IntentSender.SendIntentException
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.huawei.hms.common.ApiException
import com.huawei.hms.common.ResolvableApiException
import com.huawei.hms.location.*
import com.huawei.hms.maps.*
import com.huawei.hms.maps.model.BitmapDescriptorFactory
import com.huawei.hms.maps.model.LatLng
import com.huawei.hms.maps.model.MarkerOptions
import com.pinmyaddress.app.util.LocationLog
import com.pinmyaddress.app.R
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity(), OnMapReadyCallback {
    private val KEY = "CV8HOkhPbRnJdtx7M41XbAC1UqO9figszZH41kquJjCW44rc0Bm1XyzfXkl3+1dqEYUapB5qWgYEAmss18NNOKvxYmG7"
    private val MAPVIEW_BUNDLE_KEY = "MapViewBundleKey"
    val TAG = "LocationUpdatesIntent"
    private var hMap: HuaweiMap? = null
    var mapViewBundle: Bundle? = null
    var mLocationCallback: LocationCallback? = null
    private var mSettingsClient: SettingsClient? = null
    var mLocationRequest: LocationRequest? = null
    var myLocation: LatLng? = null
    var address: String? = null

    private var mFusedLocationProviderClient: FusedLocationProviderClient? = null

    private val viewModel: MainViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY)
        }

        initMap()
        initLocation()
        observation()

    }

    private fun observation() {
        viewModel.observeLoading.observe(this@MainActivity, Observer {

            address = if (it.isSuccess) {
                viewModel.sites.value!![0].formatAddress
            } else {
                it.message
            }

            hMap?.addMarker(
                MarkerOptions().position(LatLng(it.lat, it.lng))
                    .draggable(true)
                    .title(address)
                    .snippet("lat = ${it?.lat}" + "  lon =${it?.lng}")
                    .clusterable(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_star))
            )
        })

    }

    private fun initLocation() {
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        mSettingsClient = LocationServices.getSettingsClient(this)
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = 10000
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        if (null == mLocationCallback) {
            mLocationCallback = object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult) {
                    val locations =
                        locationResult.locations
                    if (locations.isNotEmpty()) {

                        removeLocationUpdatesWithCallback()

                        val location = locations[0]
                        myLocation = LatLng(location.latitude, location.longitude)

                        hMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 14f))
                        viewModel.getAddress(
                            myLocation!!.latitude,
                            myLocation!!.longitude
                        )


                        LocationLog.i(
                            TAG,
                            "onLocationResult location[Longitude,Latitude,Accuracy]:" + location.longitude
                                    + "," + location.latitude + "," + location.accuracy
                        )

                    }
                }

                override fun onLocationAvailability(locationAvailability: LocationAvailability) {
                    val flag = locationAvailability.isLocationAvailable
                    LocationLog.i(
                        TAG,
                        "onLocationAvailability isLocationAvailable:$flag"
                    )
                }
            }
        }
    }

    private fun requestLocationUpdatesWithCallback() {
        try {
            val builder = LocationSettingsRequest.Builder()
            builder.addLocationRequest(mLocationRequest)
            val locationSettingsRequest = builder.build()
            // Before requesting location update, invoke checkLocationSettings to check device settings.
            val locationSettingsResponseTask =
                mSettingsClient!!.checkLocationSettings(locationSettingsRequest)
            locationSettingsResponseTask.addOnSuccessListener {
                Log.i(
                    TAG,
                    "check location settings success"
                )
                mFusedLocationProviderClient!!
                    .requestLocationUpdates(
                        mLocationRequest,
                        mLocationCallback,
                        Looper.getMainLooper()
                    )
                    .addOnSuccessListener {
                        LocationLog.i(
                            TAG,
                            "requestLocationUpdatesWithCallback onSuccess"
                        )
                    }
                    .addOnFailureListener { e ->
                        LocationLog.e(
                            TAG,
                            "requestLocationUpdatesWithCallback onFailure:" + e.message
                        )
                    }
            }
                .addOnFailureListener { e ->
                    LocationLog.e(
                        TAG,
                        "checkLocationSetting onFailure:" + e.message
                    )
                    when ((e as ApiException).statusCode) {
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                           val rae = e as ResolvableApiException
                            rae.startResolutionForResult(
                                this@MainActivity,
                                0
                            )
                        } catch (sie: SendIntentException) {
                            Log.e(
                                TAG,
                                "PendingIntent unable to execute request."
                            )
                        }
                    }
                }
        } catch (e: Exception) {
            LocationLog.e(
                TAG,
                "requestLocationUpdatesWithCallback exception:" + e.message
            )
        }
    }

    private fun removeLocationUpdatesWithCallback() {
        try {
            val voidTask =
                mFusedLocationProviderClient!!.removeLocationUpdates(mLocationCallback)
            voidTask.addOnSuccessListener {
                LocationLog.i(
                    TAG,
                    "removeLocationUpdatesWithCallback onSuccess"
                )
            }
                .addOnFailureListener { e ->
                    LocationLog.e(
                        TAG,
                        "removeLocationUpdatesWithCallback onFailure:" + e.message
                    )
                }
        } catch (e: java.lang.Exception) {
            LocationLog.e(
                TAG,
                "removeLocationUpdatesWithCallback exception:" + e.message
            )
        }
    }

    override fun onMapReady(_map: HuaweiMap?) {
        Log.i("MAP_STATE", "onMapReady: ")
        hMap = _map
        hMap?.apply {
            isMyLocationEnabled = false
            uiSettings.isMyLocationButtonEnabled = false
            uiSettings.isCompassEnabled = true
            uiSettings.isRotateGesturesEnabled = true
            uiSettings.isScrollGesturesEnabled = true
            uiSettings.isScrollGesturesEnabledDuringRotateOrZoom = true
            uiSettings.isTiltGesturesEnabled = true
            uiSettings.isZoomControlsEnabled = true
            uiSettings.isIndoorLevelPickerEnabled = true
            uiSettings.isMapToolbarEnabled = true
            uiSettings.isZoomGesturesEnabled = false
            uiSettings.setAllGesturesEnabled(true)
        }
        checkPermission()

        hMap?.setOnMapClickListener {
            hMap?.clear()

            viewModel.getAddress(
                it.latitude,
                it.longitude
            )
        }
    }

    @Suppress("DEPRECATED_IDENTITY_EQUALS")
    private fun checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this@MainActivity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) !==
                PackageManager.PERMISSION_GRANTED
            ) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this@MainActivity,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                ) {
                    ActivityCompat.requestPermissions(
                        this@MainActivity,
                        arrayOf(
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        ), 1
                    )
                } else {
                    ActivityCompat.requestPermissions(
                        this@MainActivity,
                        arrayOf(
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        ), 1
                    )
                }
            } else {
                requestLocationUpdatesWithCallback()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED
                ) {

                    requestLocationUpdatesWithCallback()

                } else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show()
                }
                return
            }
        }

    }

    private fun initMap() {
        MapsInitializer.setApiKey(KEY)
        MapsInitializer.initialize(this.applicationContext)
        mapView?.onCreate(mapViewBundle)
        mapView.getMapAsync(this@MainActivity)
    }

    override fun onStart() {
        super.onStart()
        mapView?.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView?.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView?.onDestroy()
    }

    override fun onPause() {
        mapView?.onPause()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        mapView?.onResume()
    }


}