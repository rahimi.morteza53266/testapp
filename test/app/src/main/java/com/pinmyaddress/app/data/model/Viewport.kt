package com.pinmyaddress.app.data.model

data class Viewport(
    val northeast: Northeast,
    val southwest: Southwest
)