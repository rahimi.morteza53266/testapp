**MapTest App description**

This app included the following terms:
- MVVM architecture
- Koin for dependency injection
- Coroutines for asynchronous programming


**Story of code:**
- Init Map ->hms map kit
- Checking location permission
- Get geocode ->hms location kit
- Call reverse geocoding for getting location address from lat and lng
- Map cleared from markers and new marker added to the map Marker snippet **title** is **address** and **desc** is the **lat** and **lng** of location.

**Descriptions:**
- I could not connect to AppGallery
- The map is not displaying routs but the coordinates are coming . -The reverse geocoding api has authorization error called 405
-  This [link](https://developer.huawei.com/consumer/en/doc/development/HMS-Guides/hms-site-geocoding) has not anything to help but I have found [this](https://developer.huawei.com/consumer/en/doc/development/HMSCore-References-V5/webapi-reverse-geo-0000001050161968-V5) one.


